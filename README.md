<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

<div class="container">
        <h1>Instalación de Proyecto Laravel</h1>
        <p>A continuación se detallan los pasos para instalar un proyecto Laravel ya existente desde un repositorio en GitLab utilizando PHP 8.</p>
        
        Prerrequisitos
        Asegúrate de tener instalados en tu sistema:
        
            PHP 8 (puedes comprobar la versión con <code>php -v</code>)</li>
            Composer (el gestor de dependencias de PHP)</li>
            Node.js y npm (para compilar recursos frontend si el proyecto los incluye)</li>
            Git (para clonar el repositorio del proyecto)</li>
        
        
        Pasos para la instalación
        
           
                Clonar el repositorio del proyecto:
                git clone https://gitlab.com/Alewh5/cineselchele.git
cd cineselchele</code></pre>
            </li>
            <li>
                <p><strong>Instalar dependencias de PHP:</strong></p>
                <pre><code>composer install</code></pre>
            </li>
            <li>
                <p><strong>Copiar el archivo de configuración:</strong></p>
                <pre><code>cp .env.example .env</code></pre>
            </li>
            <li>
                <p><strong>Configurar el archivo <code>.env</code>:</strong></p>
                <p>Abre el archivo <code>.env</code> en tu editor de texto favorito y configura los parámetros de conexión a la base de datos y otras variables de entorno necesarias. Por ejemplo:</p>
                <pre><code>DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=cineselchele
DB_USERNAME=tu_usuario
DB_PASSWORD=tu_contraseña</code></pre>
            </li>
            <li>
                <p><strong>Generar la clave de la aplicación:</strong></p>
                <pre><code>php artisan key:generate</code></pre>
            </li>
            <li>
                <p><strong>Migrar la base de datos:</strong></p>
                <pre><code>php artisan migrate</code></pre>
            </li>
            <li>
                <p><strong>(Opcional) Poblar la base de datos con datos iniciales:</strong></p>
                <pre><code>php artisan db:seed</code></pre>
            </li>
            <li>
                <p><strong>Instalar dependencias de Node.js:</strong></p>
                <pre><code>npm install</code></pre>
            </li>
            <li>
                <p><strong>Compilar los activos frontend:</strong></p>
                <pre><code>npm run dev
// Para un entorno de producción:
npm run production</code></pre>
            </li>
            <li>
                <p><strong>Iniciar el servidor de desarrollo:</strong></p>
                <pre><code>php artisan serve</code></pre>
                <p>Accede al proyecto en tu navegador a través de <code>http://localhost:8000</code>.</p>
            </li>
        </ol>
        
        <p>Notas adicionales: Configuración de servidor web: Si vas a desplegar el proyecto en un servidor web como Apache o Nginx, asegúrate de configurar correctamente el servidor para apuntar al directorio del proyecto Laravel.
Archivos adicionales: Revisa si el proyecto incluye otras instrucciones o configuraciones especiales en un archivo o en una documentación adicional proporcionada por el equipo de desarrollo.</p>
        



## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
